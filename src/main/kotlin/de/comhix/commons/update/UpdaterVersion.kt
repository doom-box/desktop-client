package de.comhix.commons.update

/**
 * @author Benjamin Beeker
 */
data class UpdaterVersion(val buildTimestamp: Long, val url: String? = null)