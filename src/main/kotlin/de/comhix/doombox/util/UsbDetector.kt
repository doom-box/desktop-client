package de.comhix.doombox.util

import io.reactivex.Observable
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import javax.swing.filechooser.FileSystemView

/**
 * Kotlin adaption of https://github.com/samuelcampos/usbdrivedetector
 * @author Benjamin Beeker
 */
abstract class UsbDetector {
    abstract fun getUsbDrives(): Observable<UsbDevice>
}

class WindowsUsbDetector : UsbDetector() {
    companion object {
        private const val WMIC_PATH_WIN8 = "wmic"
        // Window 10 broke compatibility by removing the wbem dir from his PATH
        private val WMIC_PATH_WIN10 = System.getenv("WINDIR") + "\\System32\\wbem\\wmic"

        /**
         * wmic logicaldisk where drivetype=2 get description,deviceid,volumename
         */
        private const val CMD_WMI_ARGS = "logicaldisk where drivetype=2 get deviceid"

        private var CMD_WMI_USB: String? = null

        init {
            val wmicPath = if (System.getProperty("os.version").toFloat() >= 6.2) WMIC_PATH_WIN8 else WMIC_PATH_WIN10
            CMD_WMI_USB = "$wmicPath $CMD_WMI_ARGS"
        }
    }

    override fun getUsbDrives(): Observable<UsbDevice> {
        val process = Runtime.getRuntime()
                .exec(CMD_WMI_USB)
        val lines = with(BufferedReader(InputStreamReader(process.inputStream))) {
            Observable.fromIterable(readLines())
        }
        process.destroy()

        return lines
                .map(String::trim)
                .filter { it.isNotBlank() && it != "DeviceID" }
                .map { File(it + File.separator) }
                .filter(File::isDirectory)
                .map { it to getDisplayName(it) }
                .map {
                    UsbDevice.of(it.first,
                                 it.second)
                }
    }

    private fun getDisplayName(file: File): String? {
        val v = FileSystemView.getFileSystemView()
        var name = v.getSystemDisplayName(file)
        if (name != null) {
            val idx = name.lastIndexOf('(')
            if (idx != -1) {
                name = name.substring(0,
                                      idx)
            }
            if (name.isNotBlank()) {
                return name
            }
        }
        return null
    }
}

data class UsbDevice(val root: File,
                     val name: String) {
    companion object {
        fun of(root: File,
               name: String?): UsbDevice = UsbDevice(root,
                                                     name ?: root.name)
    }
}