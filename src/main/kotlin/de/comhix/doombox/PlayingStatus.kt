package de.comhix.doombox

import javafx.beans.property.SimpleStringProperty
import tornadofx.ItemViewModel

/**
 * @author Benjamin Beeker
 */
class PlayingStatus(val display: SimpleStringProperty = SimpleStringProperty())

class PlayingStatusModel : ItemViewModel<PlayingStatus>(PlayingStatus())