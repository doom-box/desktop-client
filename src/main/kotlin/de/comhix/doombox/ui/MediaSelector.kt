package de.comhix.doombox.ui

import de.comhix.doombox.MusicScanner
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import javafx.beans.property.SimpleIntegerProperty
import javafx.collections.FXCollections
import javafx.geometry.Pos
import javafx.scene.Parent
import tornadofx.*
import java.io.File
import java.util.concurrent.TimeUnit

/**
 * @author Benjamin Beeker
 */
class MediaSelector : View() {
    private var filelist = FXCollections.observableList<File>(mutableListOf())
    private var countdown = SimpleIntegerProperty(3)
    private val mediaController: MediaController by inject()
    private lateinit var subscription: Disposable

    override val root: Parent = vbox {
        prefHeight = 150.0
        val list = listview(filelist) {
            cellFormat {
                text = it.name
            }
        }
        hbox {
            label("Countdown") {}
            spinner(min = 0,
                    initialValue = 3,
                    amountToStepBy = 1,
                    property = countdown) {
                useMaxWidth = true
                alignment = Pos.CENTER_RIGHT

            }
            useMaxWidth = true
        }
        val button = button("start") {
            useMaxWidth = true
        }
        button.action {
            mediaController.start(list.selectedItem!!,
                                  countdown.value)
        }
        button.enableWhen {
            list.selectionModel.selectedItemProperty()
                    .isNotNull
        }
    }

    override fun onBeforeShow() {
        subscription = Observable.interval(1,
                                           TimeUnit.SECONDS)
                .flatMapSingle {
                    MusicScanner().scanForMusicFiles()
                            .toList()
                }
                .subscribe {
                    if (filelist.toSet() != it.toSet())
                        filelist.setAll(it)
                }
    }

    override fun onUndock() {
        subscription.dispose()
    }
}