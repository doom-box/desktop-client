package de.comhix.doombox.ui

import de.comhix.doombox.PlayingStatusModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import javafx.application.Platform
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import javafx.scene.Parent
import javafx.scene.control.Label
import javafx.scene.control.ToggleButton
import javafx.scene.media.MediaPlayer
import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import javafx.util.Duration
import tornadofx.*
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.logging.Level
import java.util.logging.Logger

/**
 * @author Benjamin Beeker
 */
class Player : View() {
    companion object {
        private val LOG = Logger.getLogger(Player::class.qualifiedName)
    }

    init {
        LOG.info("init player")
    }

    private val playFileModel: PlayFileModel by inject()

    private val player: MediaPlayer = MediaPlayer(playFileModel.item.media)
    private val playedPercentageProperty = player.currentTimeProperty()
            .doubleBinding { (it?.toMillis() ?: 0.0) / player.totalDuration.toMillis() }
    private val playtimeProperty = SimpleStringProperty(formatTime(player.totalDuration))

    private val playingStatus: PlayingStatusModel by inject()

    private lateinit var clockLabel: Label
    private lateinit var playButton: ToggleButton

    override val root: Parent = borderpane {
        useMaxWidth = true
        useMaxHeight = true
        prefHeight = 768.0
        prefWidth = 1366.0
        player.currentTimeProperty()
                .onChange {
                    if (it != null)
                        playtimeProperty.value = formatTime(it)
                }

        clockLabel = label(playtimeProperty) {
            alignment = Pos.CENTER_RIGHT
            useMaxWidth = true
            useMaxHeight = true
            style {
                fontWeight = FontWeight.BOLD
                fontSize = 27.em
            }
        }

        playButton = togglebutton("play",
                                  selectFirst = false) {
            alignment = Pos.TOP_LEFT
            action {
                if (!isSelected)
                    runLater {
                        stopPlayer()
                    }
                else
                    runLater {
                        clockLabel.style(append = true) {
                            textFill = Color.RED
                        }
                        startPlayer()
                    }
            }
            style {
                fontWeight = FontWeight.BOLD
                fontSize = 24.px
            }
        }

        top = playButton
        center = clockLabel

        bottom = progressbar(playedPercentageProperty) {
            useMaxWidth = true
            prefHeight = 300.0
        }

        playtimeProperty.onChange { playingStatus.item.display.value = it }
        playtimeProperty.value = "Waiting"
    }

    private fun formatTime(millis: Duration): String {
        return formatTime(millis.toMillis())
    }

    private fun formatTime(millis: Double): String {
        val negative = millis < 0
        val prefix = if (negative) "-" else " "
        val absMillis = Math.abs(millis)
        return String.format(Locale.ENGLISH,
                             "$prefix%02d:%04.1f",
                             absMillis.toInt() / 60000,
                             absMillis % 60000 / 1000.0)
    }

    private fun stopPlayer() {
        if (runningCountdown?.isDisposed != true) {
            runningCountdown?.dispose()
        }
        player.stop()
        playButton.text = "play"
    }

    private var runningCountdown: Disposable? = null

    private fun startPlayer() {
        val max = playFileModel.item.countdown * 1000L
        playButton.text = "stop"
        runningCountdown = Observable.intervalRange(0,
                                                    max / 100,
                                                    0,
                                                    100,
                                                    TimeUnit.MILLISECONDS)
                .subscribe({
                               Platform.runLater {
                                   playtimeProperty.value = formatTime(Duration.millis((it * 100 - max).toDouble()))
                               }
                           },
                           {
                               LOG.log(Level.SEVERE,
                                       "error",
                                       it)
                           },
                           {
                               Platform.runLater {
                                   clockLabel.style(append = true) {
                                       textFill = Color.BLACK
                                   }
                                   player.play()
                               }
                           })
    }
}