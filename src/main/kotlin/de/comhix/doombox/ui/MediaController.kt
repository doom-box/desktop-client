package de.comhix.doombox.ui

import tornadofx.Controller
import tornadofx.runLater
import java.io.File

/**
 * @author Benjamin Beeker
 */
class MediaController : Controller() {
    private val playFileModel: PlayFileModel by inject()

    fun start(file: File,
              countdown: Int) {
        playFileModel.item = PlayFile(file,
                                      countdown)

        runLater {
            find(MediaSelector::class).replaceWith(Player::class,
                                                   sizeToScene = true,
                                                   centerOnScreen = true)
        }
    }
}