package de.comhix.doombox.ui

import javafx.scene.media.Media
import tornadofx.ItemViewModel
import java.io.File

/**
 * @author Benjamin Beeker
 */
class PlayFile(file: File,
               val countdown: Int) {
    val media: Media = Media(file.toURI().toString())
}

class PlayFileModel : ItemViewModel<PlayFile>() 