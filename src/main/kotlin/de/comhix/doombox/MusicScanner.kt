package de.comhix.doombox

import de.comhix.doombox.util.WindowsUsbDetector
import io.reactivex.Observable
import java.io.File
import java.util.logging.Logger

/**
 * @author Benjamin Beeker
 */
class MusicScanner {
    companion object {
        private val LOG = Logger.getLogger(MusicScanner::class.java.name)
    }

    fun scanForMusicFiles(): Observable<File> {
        return scanForMusicFilesWith(WindowsUsbDetector().getUsbDrives()
                                             .flatMap {
                                                 Observable.fromIterable(it.root.listFiles().asList())
                                             })
    }

    fun scanForMusicFilesWith(source: Observable<File>): Observable<File> {
        return source
                .filter(File::isFile)
                .filter {
                    it.name.endsWith(".mp3") || it.name.endsWith(".wav")
                }
    }
}