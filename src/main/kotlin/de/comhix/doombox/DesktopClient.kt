package de.comhix.doombox

import de.comhix.commons.update.UpdaterVersion
import de.comhix.commons.updater.UpdaterInterface
import de.comhix.doombox.info.Version
import de.comhix.doombox.ui.MediaSelector
import de.comhix.doombox.web.WebServer
import io.vertx.core.Future
import io.vertx.reactivex.core.Vertx
import javafx.stage.Stage
import tornadofx.App
import tornadofx.launch
import tornadofx.onChange
import java.io.File
import java.util.logging.LogManager
import java.util.logging.Logger

/**
 * @author Benjamin Beeker
 */

class DesktopClient : App(MediaSelector::class) {
    private val vertx = Vertx.vertx()
    private val webServer = WebServer(vertx)
    private val playingStatus: PlayingStatusModel by inject()

    override fun start(stage: Stage) {
        super.start(stage)
        playingStatus.item.display.onChange {
            vertx.eventBus()
                    .publish("timer",
                             it)
        }
        val future = Future.future<Void>()
        webServer.start(future)
        future.result()
    }

    override fun stop() {
        webServer.stop()
        super.stop()
    }
}

fun main(args: Array<String>) {
    LogManager.getLogManager()
            .readConfiguration(DesktopClient::class.java.getResourceAsStream("/logging.properties"))

    val log = Logger.getGlobal()

    if (args.contains("--buildtime")) {
        System.out.println(Version.BUILD_TIMESTAMP)
        System.exit(0)
    }
    if (args.contains("--version")) {
        System.out.println(Version.BUILD_VERSION)
        System.exit(0)
    }
    if (args.contains("--builddate")) {
        System.out.println(Version.BUILD_DATE)
        System.exit(0)
    }

    log.info("AnimeDownloader started - version ${Version.BUILD_VERSION} built ${Version.BUILD_DATE}")
    log.fine("Fine logging is enabled")

    if (UpdaterInterface.tryUpdate("https://doom-box.gitlab.io/desktop-client/info",
                                   UpdaterVersion(Version.BUILD_TIMESTAMP),
                                   UpdaterVersion::url,
                                   UpdaterVersion::buildTimestamp,
                                   args.contains("--update"))) {
        val jar = File(DesktopClient::class.java.protectionDomain
                               .codeSource
                               .location
                               .path)
                .name

        log.info("update done, executing: java -jar $jar")
        Runtime.getRuntime()
                .exec("java -jar $jar")
        System.exit(0)
    }

    launch<DesktopClient>()
}