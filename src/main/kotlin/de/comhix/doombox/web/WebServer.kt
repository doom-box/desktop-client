package de.comhix.doombox.web

import io.reactivex.Single
import io.vertx.core.Future
import io.vertx.core.Handler
import io.vertx.core.buffer.Buffer
import io.vertx.ext.bridge.BridgeEventType
import io.vertx.ext.bridge.PermittedOptions
import io.vertx.ext.web.handler.sockjs.BridgeOptions
import io.vertx.reactivex.core.AbstractVerticle
import io.vertx.reactivex.core.Vertx
import io.vertx.reactivex.core.http.HttpServer
import io.vertx.reactivex.ext.web.Router
import io.vertx.reactivex.ext.web.RoutingContext
import io.vertx.reactivex.ext.web.handler.StaticHandler
import io.vertx.reactivex.ext.web.handler.sockjs.SockJSHandler
import org.apache.commons.io.IOUtils
import java.io.ByteArrayOutputStream
import java.util.concurrent.TimeUnit

/**
 * @author Benjamin Beeker
 */
class WebServer(vertx: Vertx) : AbstractVerticle() {
    init {
        this.vertx = vertx
    }

    lateinit var serverInstance: HttpServer

    override fun start(startFuture: Future<Void>) {
        val router = Router.router(vertx)

        serverInstance = vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(80) {
                    if (it.succeeded()) {
                        startFuture.complete()
                    }
                    else {
                        startFuture.fail(it.cause())
                    }
                }

        router.get("/")
                .handler(resourceHandler("/web/index.html"))
        router.get("/static/*")
                .handler(StaticHandler(io.vertx.ext.web.handler.StaticHandler.create("web")))
        router.get("/eventbus/*")
                .handler(eventBusHandler())
                .handler {
                    vertx.eventBus()
                            .publish("timer",
                                     "connected ${it.request().connection().remoteAddress().host()}")
                }
        vertx.eventBus()
                .publish("timer",
                         "server online")
    }

    private fun eventBusHandler(): SockJSHandler {
        val options = BridgeOptions()
                .addOutboundPermitted(PermittedOptions().setAddress("timer"))
        return SockJSHandler.create(vertx)
                .bridge(options
                ) { event ->
                    if (event.type() === BridgeEventType.SOCKET_CREATED) {
                        Single.just("connected")
                                .delay(1,
                                       TimeUnit.SECONDS)
                                .subscribe { _: String ->
                                    vertx.eventBus()
                                            .publish("timer",
                                                     "connected")
                                }
                    }
                    event.complete(true)
                }
    }

    private fun resourceHandler(resource: String): Handler<RoutingContext> {
        return Handler {
            val file = WebServer::class.java.getResourceAsStream(resource)
            val baos = ByteArrayOutputStream()
            IOUtils.copy(file,
                         baos)

            it.response()
                    .isChunked = true
            it.response()
                    .write(io.vertx.reactivex.core.buffer.Buffer.newInstance(Buffer.buffer(baos.toByteArray())))
            it.response()
                    .end()
        }
    }

    override fun stop() {
        serverInstance.close()
        vertx.close()
        super.stop()
    }
}