package de.comhix.doombox

import io.reactivex.Observable
import org.amshove.kluent.`should be equal to`
import org.testng.annotations.Test
import java.io.File
import java.util.logging.Logger

/**
 * @author Benjamin Beeker
 */
object MusicScannerTest {
    private val LOG = Logger.getLogger(MusicScannerTest::class.simpleName)

    @Test
    fun `scan returns mp3 file`() {
        LOG.info("scan returns mp3 file")

        // given
        val filename = "04 Barrel Roll [PrototypeRaptor].mp3"
        val path = MusicScannerTest::class.java.getResource("/$filename")
                .toURI()
        val fileList = File(path).parentFile.listFiles()
                .toList()

        // when
        val foundFiles = MusicScanner().scanForMusicFilesWith(Observable.fromIterable(fileList))
                .blockingIterable()
                .toList()

        // then
        foundFiles.size `should be equal to` 1
        foundFiles[0].name `should be equal to` filename
    }
}